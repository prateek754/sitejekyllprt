---
layout: page
title: Contact
permalink: /contact/
---

**Address: <br>
Department of Psychology <br>
P-551, Biological Sciences Building <br>
University of Alberta <br>
Edmonton, Canada**

**Email id: <br>
[prateek6289@gmail.com](mailto:prateek6289@gmail.com) <br>
[prateek@prateeksahu.com](mailto:prateek@prateeksahu.com) <br>
[psahu@ualberta.ca](mailto:psahu@ualberta.ca)**